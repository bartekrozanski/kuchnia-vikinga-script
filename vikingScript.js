javascript: (() => {
  const dobreFrazy = ["sałatk"];
  const zleFrazy = ["pieczar", "grzyb", "rodzyn"];

  const goodPhraseRegexes = dobreFrazy.map((str) => new RegExp(str, "i"));
  const badPhraseRegexes = zleFrazy.map((str) => new RegExp(str, "i"));

  function isUserOnCorrectPage() {
    return window.location.pathname === "/";
  }

  function openPopperForMealElem(mealElem) {
    mealElem.getElementsByTagName("i").item(0).click();
    return getPopper();
  }

  function closePopper(popper) {
    popper.getElementsByClassName("details-exit").item(0).click();
  }

  function getPopper() {
    return document.getElementsByClassName("meal-details-popper").item(0);
  }

  function hotInPopper(popper) {
    const numOfFireIcons = popper.getElementsByClassName("fa-fire").length;
    if (numOfFireIcons > 1) {
      throw new Error("Number of fire icons should not be bigger than one");
    }
    return numOfFireIcons === 1;
  }

  function coldInPopper(popper) {
    const numOfSnowflakeIcons =
      popper.getElementsByClassName("fa-snowflake").length;
    if (numOfSnowflakeIcons > 1) {
      throw new Error(
        "Number of snowflake icons should not be bigger than one"
      );
    }
    return numOfSnowflakeIcons === 1;
  }

  function extractPopperText(popper) {
    return popper.textContent;
  }

  function addText(node, text, color) {
    const newElem = document.createElement("div");
    newElem.style.color = color;
    newElem.textContent = text;
    node.append(newElem);
  }

  function stringifyRegex(regex) {
    return regex.toString().slice(1, -2);
  }

  function fixButtonPositionIfItExists(mealElem) {
    const button = mealElem
      .getElementsByClassName("single-meal-buttons")
      .item(0);
    if (button === null) return;
    button.style.top = "16px";
    button.style.right = "16px";
    button.style.transform = "unset";
  }

  function main() {
    if (!isUserOnCorrectPage()) {
      alert("Jesteś na złej stronie. Najpierw wejdź w zakładkę 'zamówienia'.");
      return;
    }

    const drawer = document.getElementsByClassName("drawer").item(0);

    const observerOptions = {
      childList: true,
      subtree: true,
    };

    const callback = (records, observer) => {
      const meals = drawer.getElementsByClassName("single-meal");
      if (meals.length === 0) return;
      observer.disconnect();
      [...meals].forEach((meal) => {
        fixButtonPositionIfItExists(meal);
        const popper = openPopperForMealElem(meal);
        if (hotInPopper(popper)) {
          addText(meal, "Na ciepło", "#e79a00");
        }
        if (coldInPopper(popper)) {
          addText(meal, "Na zimno", "#3399ff");
        }
        const popperText = extractPopperText(popper);

        const foundGoodPhrasesString = goodPhraseRegexes
          .flatMap((regex) => {
            if (regex.test(popperText)) return stringifyRegex(regex);
            return [];
          })
          .join(", ");
        if (foundGoodPhrasesString !== "") {
          addText(meal, foundGoodPhrasesString, "#29a75e");
        }

        const foundBadPhrasesString = badPhraseRegexes
          .flatMap((regex) => {
            if (regex.test(popperText)) return stringifyRegex(regex);
            return [];
          })
          .join(", ");
        if (foundBadPhrasesString !== "") {
          addText(meal, foundBadPhrasesString, "#ff633c");
        }

        if (foundGoodPhrasesString !== "" && foundBadPhrasesString === "") {
          meal.style.backgroundColor = "#22ff2222";
        }
        if (foundBadPhrasesString !== "") {
          meal.style.backgroundColor = "#ff222222";
        }

        closePopper(popper);
      });
    };

    const mealChangeObserver = new MutationObserver(callback);

    drawerCallback = (records, observer) => {
      const drawer = records[0].target;
      if ([...drawer.classList].includes("is-open")) {
        mealChangeObserver.observe(drawer, observerOptions);
      }
    };
    const drawerObserverOptions = {
      attributes: true,
      attributeFilter: ["class"],
    };
    const drawerObserver = new MutationObserver(drawerCallback);
    drawerObserver.observe(drawer, drawerObserverOptions);
    alert("Działam! Zacznij teraz wybierać posiłki.");
  }

  main();
})();
